##########################################################################
# File Name: run.sh
# Author: hcp6897
# mail: hcp6798@163.com
# Created Time: Thu 13 Oct 2022 03:27:08 PM CST
#########################################################################
#!/bin/bash

make clean
mkdir -p build
cd build
cmake -G Ninja ..
ninja

